from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def show_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipts': receipts
    }
    return render(request, 'receipts/list.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form': form
    }
    return render(request, 'receipts/create.html', context)

@login_required
def show_account(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        'account': account
    }
    return render(request, 'accounts/account.html', context)

@login_required
def show_expense(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context ={
        'expense': expense
    }
    return render(request, 'expense/expense.html', context)

@login_required
def create_expense(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context = {
        'form': form
    }
    return render(request, 'expense/create.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        'form': form
    }
    return render(request, 'accounts/create.html', context)
