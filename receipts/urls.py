from django.urls import path
from receipts.views import show_receipts, create_receipt, show_account, show_expense, create_expense, create_account

urlpatterns = [
    path('', show_receipts, name='home'),
    path('create/', create_receipt, name= 'create_receipt'),
    path('categories/', show_expense, name='category_list'),
    path('accounts/', show_account, name='account_list'),
    path('categories/create/', create_expense, name='create_category'),
    path('accounts/create/', create_account, name='create_account'),
]
